from main import function


def test_1():
    assert function(45, "СМ", "М") == 0.45


def test_2():
    assert function(12, "КМ", "ММ") == 12000000


def test_3():
    assert function(1200, "ММ", "М") == 1.2


test_1()
test_2()
test_3()
